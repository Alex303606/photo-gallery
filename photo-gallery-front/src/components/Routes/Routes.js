import React from 'react';
import { Route, Switch } from "react-router-dom";
import Artists from "../../containers/Gallery/Gallery";
import Register from "../../containers/Register/Register";
import AddPhoto from "../../containers/AddPhoto/AddPhoto";
import User from "../../containers/User/User";

const Routes = () => {
	return (
		<Switch>
			<Route path="/" exact component={Artists}/>
			<Route path="/register" exact component={Register}/>
			<Route path="/add_photo" exact component={AddPhoto}/>
			<Route path="/users:id" exact component={User}/>
		</Switch>
	);
};

export default Routes;
