import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { toggleModalPhoto } from "../../store/actions/photos";
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import './PhotoModal.css';
import { MdClose } from "react-icons/lib/md/index";

class PhotoModal extends Component {
	render() {
		let image = notFound;
		
		if (this.props.currentImage) image = `${config.apiUrl}uploads/${this.props.currentImage}`;
		return (
			<Fragment>
				<div className="wrapper"
				     style={this.props.showModalPhoto ? {display: 'block'} : {display: 'none'}}/>
				<div className="modal_login modal_photo"
				     style={this.props.showModalPhoto ? {transform: 'translateX(-50%) translateY(-50%)'} : {transform: 'translateX(-50%) translateY(-500%)'}}>
					<button className="btn_close" onClick={this.props.toggleModalPhoto}><MdClose/></button>
					<img src={image} alt="big_image"/>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	showModalPhoto: state.photos.showModalPhoto,
	currentImage: state.photos.currentImage
});

const mapDispatchToProps = dispatch => ({
	toggleModalPhoto: () => dispatch(toggleModalPhoto())
});

export default connect(mapStateToProps, mapDispatchToProps)(PhotoModal);

