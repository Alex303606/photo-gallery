import React from 'react';
import PropTypes from 'prop-types';
import notFound from '../../assets/images/not-found.jpeg';
import config from "../../config";
import './Photo.css';
import { NavLink } from "react-router-dom";

const Photo = props => {
	
	let image = `url(${notFound})`;
	
	if (props.image) image = `url(${config.apiUrl}uploads/${props.image})`;
	
	return (
		<div className="photo__item">
			<div className="image" onClick={props.click}
			     style={{
				     backgroundImage: image,
				     backgroundRepeat: 'no-repeat',
				     backgroundSize: 'cover',
				     backgroundPosition: 'center'
			     }}/>
			<div className="title"><b>Title:</b> {props.title}</div>
			{props.username &&
				<NavLink to={"/users" + props.user}>
					<div className="title author"><b>Author:</b> {props.username}</div>
				</NavLink>
			}
			{
				props.delete && <button onClick={props.delete} className="btn delete_photo">Delete</button>
			}
		</div>
	);
};

Photo.propTypes = {
	image: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	click: PropTypes.func.isRequired
};

export default Photo;
