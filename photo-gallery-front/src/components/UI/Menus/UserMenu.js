import React, { Fragment } from 'react';
import { NavLink } from "react-router-dom";
import { FaSignOut } from "react-icons/lib/fa/index";
import { MdPhoto } from "react-icons/lib/md/index";

const UserMenu = ({user, avatar, logout}) => {
	return (
		<Fragment>
			<li><NavLink to="/add_photo"><MdPhoto/><span>Add photo</span></NavLink></li>
			<li onClick={logout}><a><FaSignOut/><span>Logout</span></a></li>
			<li className='user_menu'>
				<NavLink to={"/users" + user._id}>
					<img src={avatar} alt="" className="user_avatar"/>{user.username}
				</NavLink>
			</li>
		</Fragment>
	)
};

export default UserMenu;
