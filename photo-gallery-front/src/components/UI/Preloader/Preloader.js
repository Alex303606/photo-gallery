import React from 'react';
import preloader from '../../../assets/images/preloader.gif';

const Preloader = () => {
	return (
		<div className="preloader"
		     style={{
			     backgroundImage: `url(${preloader})`,
			     backgroundSize: 'cover',
			     backgroundPosition: 'center',
			     backgroundRepeat: 'no-repeat',
			     width: '100%',
			     height: '500px'
		     }}
		/>
	);
};

export default Preloader;
