import React, { Component } from 'react';
import './Header.css';
import { NavLink } from "react-router-dom";
import AnonymousMenu from "../Menus/AnonymousMenu";
import UserMenu from "../Menus/UserMenu";
import { connect } from "react-redux";
import { logoutUser } from "../../../store/actions/users";
import avatar from '../../../assets/images/user.png';

class Header extends Component {
	render() {
		return (
			<header>
				<div className="container">
					<NavLink to='/' className="header__logo">
						<h1>Photo Gallery</h1>
					</NavLink>
					<ul>
						{this.props.user ? <UserMenu avatar={avatar} logout={this.props.logoutUser} user={this.props.user}/> : <AnonymousMenu/>}
					</ul>
				</div>
			</header>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user
});

const mapDispatchToProps = dispatch => ({
	logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
