import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { deletePhoto, fetchUserPhotos, toggleModalPhoto } from "../../store/actions/photos";
import './User.css';
import Photo from "../../components/Photo/Photo";

class User extends Component {
	
	componentDidMount() {
		this.props.fetchUserPhotos(this.props.match.params.id);
	}
	
	render() {
		return (
			<Fragment>
				<div className="container gallery">
					<div className="gallery__items">
						{
							this.props.currentPhotos.map(photo => (
								<Photo
									click={() => this.props.toggleModalPhoto(photo.image)}
									key={photo._id}
									title={photo.title}
									image={photo.image}
									delete={(this.props.user && (photo.user._id === this.props.user._id))
										? () => this.props.deletePhoto(photo._id,photo.user._id)
										: null
									}
								/>
							))
						}
					</div>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	currentPhotos: state.photos.currentPhotos,
	user: state.user.user
});

const mapDispatchToProps = dispatch => ({
	fetchUserPhotos: id => dispatch(fetchUserPhotos(id)),
	toggleModalPhoto: image => dispatch(toggleModalPhoto(image)),
	deletePhoto: (id,user) => dispatch(deletePhoto(id,user))
});

export default connect(mapStateToProps, mapDispatchToProps)(User);
