import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchPhotos, toggleModalPhoto } from "../../store/actions/photos";
import Photo from "../../components/Photo/Photo";
import './Gallery.css';

class Gallery extends Component {
	
	componentDidMount() {
		this.props.fetchPhotos();
	}
	
	render() {
		return (
			<Fragment>
				<div className="container gallery">
					<div className="gallery__items">
						{
							this.props.photos.map(photo => (
								<Photo
									click={() => this.props.toggleModalPhoto(photo.image)}
									key={photo._id}
									title={photo.title}
									image={photo.image}
									username={photo.user.username}
									user={photo.user._id}
								/>
							))
						}
					</div>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	photos: state.photos.photos
});

const mapDispatchToProps = dispatch => ({
	fetchPhotos: () => dispatch(fetchPhotos()),
	toggleModalPhoto: (image) => dispatch(toggleModalPhoto(image))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
