import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaImage } from "react-icons/lib/fa/index";
import notFound from '../../assets/images/not-found.jpeg';
import './AddPhoto.css';
import { MdSave } from "react-icons/lib/md/index";
import { addPhoto } from "../../store/actions/photos";
import { toggleModalLogin } from "../../store/actions/users";

class AddPhoto extends Component {
	
	componentDidMount() {
		if (!this.props.user) {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	}
	
	state = {
		image: '',
		title: '',
		preview: null
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.addPhoto(formData);
	};
	
	render() {
		let image = notFound;
		return (
			<div className="container">
				<h1>Add photo</h1>
				<div className="add_form">
					<div className="add_form_image">
						<img src={this.state.preview || image} alt="post"/>
					</div>
					<div className="add_form_info">
						<form onSubmit={this.submitFormHandler}>
							<div className="row">
								<label htmlFor="title">Title:</label>
								<input onChange={this.inputChangeHandler} value={this.state.title} id="title" name="title"
								       type="text"/>
								{(this.props.error && this.props.error.errors && this.props.error.errors.title)
								&& <span className="error">Title is required</span>
								}
							</div>
							<div className="row">
								<label className="addImage" htmlFor="image"><FaImage/><span>Add image</span></label>
								<input id="image"
								       style={{display: 'none'}}
								       name="image"
								       onChange={this.fileChangeHandler}
								       type="file"/>
								{(this.props.error && this.props.error.errors && this.props.error.errors.image)
								&& <span className="error">Image is required</span>
								}
							</div>
							<button className="btn save_photo"><MdSave/><span>Save photo</span></button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	error: state.photos.addPhotoError
});

const mapDispatchToProps = dispatch => ({
	addPhoto: photoData => dispatch(addPhoto(photoData)),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto);
