import React, { Component, Fragment } from 'react';
import Header from "./components/UI/Header/Header";
import Routes from "./components/Routes/Routes";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import PhotoModal from "./components/PhotoModal/PhotoModal";

class App extends Component {
	render() {
		return (
			<Fragment>
				<NotificationContainer/>
				<Header/>
				<PhotoModal/>
				<Routes user={this.props.user}/>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user
});

export default withRouter(connect(mapStateToProps)(App));

