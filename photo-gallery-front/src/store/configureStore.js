import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import thunkMiddleware from "redux-thunk";
import { loadState, saveState } from "./localStorage";
import photosReducer from "./reducers/photos";
import userReducer from "./reducers/users";

const rootReducer = combineReducers({
	photos: photosReducer,
	user: userReducer,
	routing: routerReducer
});

export const history = createHistory();

const middleware = [
	thunkMiddleware,
	routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
	saveState({
		user: store.getState().user
	});
});

export default store;