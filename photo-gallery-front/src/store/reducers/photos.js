import {
	FETCH_USER_PHOTOS_SUCCESS,
	FETCH_PHOTOS_SUCCESS,
	TOGGLE_MODAL_PHOTO, ADD_PHOTO_FAILURE, ADD_PHOTO_SUCCESS
} from "../actions/actionType";

const initialState = {
	photos: [],
	currentPhotos: [],
	showModalPhoto: false,
	currentImage: null,
	addPhotoError: null
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_PHOTOS_SUCCESS:
			return {...state, photos: action.photos};
		case ADD_PHOTO_FAILURE:
			return {...state, addPhotoError: action.error};
		case ADD_PHOTO_SUCCESS:
			return {...state, addPhotoError: null};
		case FETCH_USER_PHOTOS_SUCCESS:
			return {...state, currentPhotos: action.photos};
		case TOGGLE_MODAL_PHOTO:
			return {...state, showModalPhoto: !state.showModalPhoto, currentImage: action.image};
		default:
			return state;
	}
};

export default reducer;