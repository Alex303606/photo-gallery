import {
	ADD_PHOTO_FAILURE,
	ADD_PHOTO_SUCCESS,
	FETCH_PHOTOS_SUCCESS,
	FETCH_USER_PHOTOS_SUCCESS,
	TOGGLE_MODAL_PHOTO
} from "./actionType";
import axios from '../../axios-api';
import { push } from "react-router-redux";

export const fetchPhotosSuccess = photos => {
	return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
	return dispatch => {
		axios.get('/photos').then(
			response => dispatch(fetchPhotosSuccess(response.data))
		);
	}
};

export const addPhoto = photoData => {
	return (dispatch) => {
		axios.post('/photos', photoData).then(
			response => {
				dispatch(addPhotoSuccess());
				dispatch(push('/'));
			},
			error => {
				dispatch(addPhotoFailure(error.response.data));
			}
		)
	}
};

const addPhotoFailure = error => {
	return {type: ADD_PHOTO_FAILURE, error};
};

const addPhotoSuccess = () => {
	return {type: ADD_PHOTO_SUCCESS};
};

export const toggleModalPhoto = (image = null) => {
	return (dispatch) => {
		dispatch(togglePhotoModal(image));
	};
};

export const togglePhotoModal = image => {
	return {type: TOGGLE_MODAL_PHOTO, image};
};
export const fetchUserPhotos = id => {
	return dispatch => {
		return axios.get(`/photos/${id}`).then(response => {
			dispatch(fetchUserPhotosSuccess(response.data));
		});
	}
};

const fetchUserPhotosSuccess = photos => {
	return {type: FETCH_USER_PHOTOS_SUCCESS, photos};
};

export const deletePhoto = (id, user) => {
	return dispatch => {
		axios.delete(`/photos/${id}`).then(
			() => dispatch(fetchUserPhotos(user))
		);
	}
};