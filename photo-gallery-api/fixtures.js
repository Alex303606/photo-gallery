const mongoose = require('mongoose');
const config = require('./config');
const fs = require('fs');
const Photos = require('./models/Photos');
const User = require('./models/Users');
const dirpath = './public/uploads';

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const files = fs.readdirSync(dirpath).sort();


db.once('open', async () => {
	try {
		await db.dropCollection('photos');
		await db.dropCollection('users');
	} catch (e) {
		console.log('Collection were not present, skipping drop...');
	}
	
	const user = [];

	for (let i = 1; i < 11; i++) {

		user[i] = await User.create({
			username: 'user' + i,
			password: 666 * i
		});

		await Photos.create({
			image: files[i],
			title: 'Some title',
			user: user[i]._id
		});
	}
	db.close();
});