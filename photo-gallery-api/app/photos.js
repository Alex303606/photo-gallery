const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Photos = require('../models/Photos');
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	
	router.post('/', [auth, upload.single('image')], (req, res) => {
		const photoData = req.body;
		if (req.file) {
			photoData.image = req.file.filename;
		} else {
			photoData.image = null;
		}
		
		photoData.user = req.user._id;
		const photo = new Photos(photoData);
		
		photo.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.get('/', (req, res) => {
		Photos.find().populate('user')
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.get('/:id', (req, res) => {
		Photos.find({user: req.params.id}).populate('user')
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => {
			res.sendStatus(500);
		});
	});
	
	router.delete('/:id', auth, (req, res) => {
		Photos.findOne({_id: req.params.id}).populate('user').then(photo => {
			if(req.user._id.equals(photo.user._id)) {
				Photos.deleteOne({_id: req.params.id})
				.then(result => {
					if (result) res.send(result);
					else res.sendStatus(404);
				})
				.catch(() => res.sendStatus(500));
			} else {
				res.status(403).send({message: {error: 'You are not the master!'}});
			}
		});
	});
	
	return router;
};

module.exports = createRouter;