const path = require('path');

const rootPath = __dirname;

module.exports = {
	rootPath,
	uploadPath: path.join(rootPath, '/public/uploads'),
	db: {
		url: 'mongodb://localhost:27017',
		name: 'photo-gallery'
	},
	jwt: {
		secret: 'some kinda very secret key',
		expires: '7d'
	},
	facebook: {
		facebookAppId: '1759721714088730',
		appSecret: '79b1905ed3a254bb5bf74c1762b47247'
	}
};