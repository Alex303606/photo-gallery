const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhotosSchema = new Schema({
	title: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'Users',
		required: true
	}
});

const Photos = mongoose.model('Photos', PhotosSchema);

module.exports = Photos;